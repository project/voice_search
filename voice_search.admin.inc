<?php

/**
 * @file
 * Administration page callbacks for the Voice Search module.
 */

/**
 * Page callback that shows an overview of voice search configuration.
 */
function voice_search_admin_settings_form() {
  $form = array();
  drupal_set_title(t('Voice Search Configuration'));
  $form['voice_search_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Voice Search Configuration'),
    '#weight' => 5,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['voice_search_config']['voice_search_status'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('voice_search_status'),
    '#required' => FALSE,
    '#title' => t('Enable'),
  );

  return system_settings_form($form);
}
