<?php

/**
 * @file
 * Voice Search Module.
 */

global $base_url;
?>
<div class="speak-now-section" id="voice_search">
    <div class="speak-now-text" id="voice-search-speak-now-default-text">Speak Now..</div>
    <div class="speak-now-text" id="voice-search-speak-now-search-text"></div>
    <div class="speak-now--re-try-text" id="voice-search-speak-now-retry-text">Didn't get, <a href="javascript:void(0);" id="voice-search-retry">try again</a></div>
    <div class="speak-now-icon">
        <a href="#" class="voice-icon">
            <img src="<?php print $base_url; ?>/<?php print drupal_get_path('module', 'voice_search') ?>/images/speaker_icon.png" />
        </a>
    </div>
    <img src="<?php print $base_url; ?>/<?php print drupal_get_path('module', 'voice_search') ?>/images/close_voice_search.png" class="close-voice-search">
</div>
