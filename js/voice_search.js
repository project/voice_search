
/**
 * @file
 * Javascript functions.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.voice_search = {
    attach: function (context, settings) {
      if (window.hasOwnProperty('webkitSpeechRecognition') === false) {
        $(".voice-search-block").find('.search-icon').remove();
        $("#voice_search").remove();
      }
      $('.search-icon').click(function() {
        $('#voice_search').show();
        if (window.hasOwnProperty('webkitSpeechRecognition')) {

          var recognition = new webkitSpeechRecognition();

          recognition.continuous = false;
          recognition.interimResults = false;

          recognition.lang = "en-US";
          recognition.start();

          recognition.onstart = function() {
            //Listening (capturing voice from audio input) started.
            //This is a good place to give the user visual feedback about that (i.e. flash a red light, etc.)
          };

          recognition.onend = function() {
            //Again – give the user feedback that you are not listening anymore. If you wish to achieve continuous recognition – you can write a script to start the recognizer again here.
          };
          recognition.onresult = function(e) {
            $('#voice-search-speak-now-default-text').hide();
            $('#voice-search-speak-now-search-text').show();
            $('#voice-search-speak-now-retry-text').hide();
            $('#voice-search-speak-now-search-text').html(e.results[0][0].transcript);
            recognition.stop();
            setTimeout(function() {
              $('.voice-search-block input[type="text"]').val(e.results[0][0].transcript);
              $('.voice-search-block').parents('form').submit();
            }, 2000);
          };

          recognition.onerror = function(e) {
            recognition.stop();
            $('#voice-search-speak-now-default-text').hide();
            $('#voice-search-speak-now-search-text').hide();
            $('#voice-search-speak-now-retry-text').show();
          }

        }
      });

      $('#voice-search-retry').click(function() {
        $('#voice-search-speak-now-default-text').show();
        $('#voice-search-speak-now-search-text').hide();
        $('#voice-search-speak-now-retry-text').hide();
        if (window.hasOwnProperty('webkitSpeechRecognition')) {

          var recognition = new webkitSpeechRecognition();

          recognition.continuous = false;
          recognition.interimResults = false;

          recognition.lang = "en-US";
          recognition.start();

          recognition.onresult = function(e) {
            $('#voice-search-speak-now-default-text').hide();
            $('#voice-search-speak-now-search-text').show();
            $('#voice-search-speak-now-retry-text').hide();
            $('#voice-search-speak-now-search-text').html(e.results[0][0].transcript);
            recognition.stop();
            setTimeout(function() {
              $('.voice-search-block input[type="text"]').val(e.results[0][0].transcript);
              $('.voice-search-block').parents('form').submit();
            }, 5000);

          };

          recognition.onerror = function(e) {
            recognition.stop();
            $('#voice-search-speak-now-default-text').hide();
            $('#voice-search-speak-now-search-text').hide();
            $('#voice-search-speak-now-retry-text').show();
          }

        }
      });
      $('.close-voice-search').click(function() {
        var recognition = new webkitSpeechRecognition();
        recognition.stop();
        $('#voice_search').hide();
      });
    }
  };
})(jQuery);
